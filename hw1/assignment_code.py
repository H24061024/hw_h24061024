import time
import RPi.GPIO as GPIO

SWITCH_PIN = [15]
LED_PIN = [37,3,5,11]
GPIO.setmode(GPIO.BOARD)

GPIO.setup(SWITCH_PIN, GPIO.IN)
GPIO.setup(LED_PIN, GPIO.OUT)

try:
	count = 0
	while True:
		if GPIO.input(SWITCH_PIN[0]) == GPIO.HIGH:
			count += 1
		if count >= 16 :
			count = 0
		temp = format(count, "b")
		if len(temp)<2:
			temp="000"+temp
		if len(temp)<3:
			temp="00"+temp
		if len(temp)<4:
			temp="0"+temp
		print(temp)	
		if temp[0]=="1":
			print("Y")
			GPIO.output(LED_PIN[0], GPIO.HIGH)
		else:
			GPIO.output(LED_PIN[0], GPIO.LOW)
		if temp[1]=="1":
			print("G")
			GPIO.output(LED_PIN[1], GPIO.HIGH)
		else:
			GPIO.output(LED_PIN[1], GPIO.LOW)
		if temp[2]=="1":
			print("R")
			GPIO.output(LED_PIN[2], GPIO.HIGH)
		else:
			GPIO.output(LED_PIN[2], GPIO.LOW)
		if temp[3]=="1":
			print("B")
			GPIO.output(LED_PIN[3], GPIO.HIGH)
		else:
			GPIO.output(LED_PIN[3], GPIO.LOW)
		time.sleep(0.5)
except KeyboardInterrupt:
    print("kb")
finally:
    GPIO.cleanup()