import time
import RPi.GPIO as GPIO
SWITCH_PIN = [11, 13, 15]
LED_PIN = [3, 5, 7]
GPIO.setmode(GPIO.BOARD)
GPIO.setup(SWITCH_PIN, GPIO.IN)
GPIO.setup(LED_PIN, GPIO.OUT)
try:
    while True:
        if GPIO.input(SWITCH_PIN[0]) == GPIO.HIGH:
            GPIO.output(LED_PIN[0], GPIO.HIGH)
        else:
            GPIO.output(LED_PIN[0], GPIO.LOW)
        if GPIO.input(SWITCH_PIN[1]) == GPIO.HIGH:
            GPIO.output(LED_PIN[1], GPIO.HIGH)
        else:
            GPIO.output(LED_PIN[1], GPIO.LOW)
        if GPIO.input(SWITCH_PIN[2]) == GPIO.HIGH:
            GPIO.output(LED_PIN[2], GPIO.HIGH)
        else:
            GPIO.output(LED_PIN[2], GPIO.LOW)
        time.sleep(0.5)
except KeyboardInterrupt:
    print("kb")
finally:
    GPIO.cleanup()