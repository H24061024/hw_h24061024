from picamera.array import PiRGBArray
from picamera import PiCamera
import cv2
import time
#####################################setting camera
camera = PiCamera()
camera.resolution = (320, 240)
try:
	with PiRGBArray(camera) as output:
		camera.capture(output, format='bgr')
		time=time.strftime("%m%d_%H%M.jpg", time.localtime()) 
		img = output.array #影像(img)在此被儲存為array的格式，可以接續讓opencv做處理
		net = cv2.dnn.readNetFromTorch('starry_night.t7') #讀取風格檔，這裡讀入"星空"的風格
		net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
		(h, w) = img.shape[:2]
		blob = cv2.dnn.blobFromImage(img, 1.0, (w,h), (103.939, 116.779, 123.680), swapRB=False, crop=False) #把影像修改成神經網路可以使用的格式
		net.setInput(blob) #把影像丟入模型做風格轉換
		out = net.forward() #開始轉換!
		out = out.reshape(3, out.shape[2], out.shape[3])
		out[0] += 103.939
		out[1] += 116.779
		out[2] += 123.68
		out = out.transpose(1, 2, 0)
		cv2.imwrite(time,out)
		cv2.imwrite("origin.jpg",img)
		cv2.imshow("a", out)
		cv2.waitKey(0)		
#####################################
except KeyboardInterrupt:
	print('interrupt')
finally:
	# 程式結束之後記得關閉相機，同時把剛剛呼叫出來的視窗關閉喔!
	camera.close()
	cv2.destroyAllWindows()